#include <iostream>
#include <cmath>

int main() {

	double integerOne = 0;
	double integerTwo = 0;
	double integerThree = 0;
	double integerFour = 0;
	int selection = 0;
	int selectionTwo = 0;
	int selectionThree = 0;
	int selectionFour = 0;


	while (selection != 8, selectionTwo != 8, selectionThree != 8, selectionFour != 8) {


		std::cout << "Current operations that you may use. Press 8 if you'd like to exit. \n\n"
				  << "\n1. Addition"
				  << "\n2. Subtraction"
				  << "\n3. Multiplication"
				  << "\n4. Division"
				  << "\n5. Squaring"
				  << "\n6. Square Root"
				  << "\n\n7. Page 2"
				  << "\n\nPlease type the number of your operation into the terminal: "; std::cin >> selection;


		if (selection == 1) {

			std::cout << "\n\nPlease enter your number for addition: ";
			std::cin >> integerOne;
			std::cout << "+";
			std::cin >> integerTwo;
			std::cout << " = " << (integerOne + integerTwo) << "\n";

		}
		else if (selection == 2) {

			std::cout << "\n\nPlease enter your number for subtraction: ";
			std::cin >> integerOne;
			std::cout << "-";
			std::cin >> integerTwo;
			std::cout << " = " << (integerOne - integerTwo) << "\n";

		}
		else if (selection == 3) {

			std::cout << "\n\nPlease enter your number for multiplication: ";
			std::cin >> integerOne;
			std::cout << " * ";
			std::cin >> integerTwo;
			std::cout << " = " << (integerOne * integerTwo) << "\n";

		}
		else if (selection == 4) {

			std::cout << "\n\nPlease enter your numbers for division: ";
			std::cin >> integerOne;
			std::cout << "/";
			std::cin >> integerTwo;
			std::cout << " = " << (integerOne / integerTwo) << "\n";

		}
		else if (selection == 5) {

			std::cout << "\n\nPlease enter the number you'd like to square: ";
			std::cin >> integerOne;
			std::cout << "\n\n" << integerOne << " squared is " << (integerOne * integerOne);

		}
		else if (selection == 6) {

			std::cout << "\n\nPlease enter the number you want to find the root for: ";
			std::cin >> integerOne;
			std::cout << "\n\nThe square root of your number is " << sqrt(integerOne);

		}
		else if (selection == 7) {

			std::cout << "\n1. Fahrenheit to Celsius Converter"
					  << "\n2. BMI Calculator"
					  << "\n3. Exponents"
					  << "\n4. Cube Roots"
					  << "\n5. Sin"
					  << "\n6. Fraction to Percentage Converter"
					  << "\n\n7. Page 3"
					  << "\n\n\nPlease select the number of your operation: ";
			std::cin >> selectionTwo;

			if (selectionTwo == 1) {

				std::cout << "\nEnter degrees in fahrenheit: ";
				std::cin >> integerThree;
				std::cout << integerThree << " degrees fahrenheit is " << ((integerThree - 32) * 5 / 9) << " degrees Celsius\n\n";

			}
			else if (selectionTwo == 2) {

				std::cout << "\n\nPlease enter your weight in kg and height in meters.\n";
				std::cout << "\nWeight: ";
				std::cin >> integerThree;
				std::cout << "\nHeight: ";
				std::cin >> integerFour;
				std::cout << "Your BMI is: " << integerThree / (integerFour * integerFour) << "\n\n";

			}
			else if (selectionTwo == 3) {

				std::cout << "\nBase: ";
				std::cin >> selection;
				std::cout << "\nMultiplier: ";
				std::cin >> selectionTwo;
				std::cout << selection << " to the power of " << selectionTwo << " is " << pow(selection, selectionTwo);

			}
			else if (selectionTwo == 4) {

				std::cout << "\nEnter the number you'd like the cube root for: ";
				std::cin >> selection;
				std::cout << "\n\nThe cube root of " << selection << " is " << cbrt(selection);

			}
			else if (selectionTwo == 5) {

				std::cout << "\nEnter number: ";
				std::cin >> integerOne;
				std::cout << "\nsin(" << integerOne << ") = " << sin(integerOne);

			}
			else if (selectionTwo == 6) {

				std::cout << "\n\nEnter numerator: ";
				std::cin >> integerOne;
				std::cout << "\n\nEnter denominator: ";
				std::cin >> integerTwo;
				std::cout << integerOne << " / " << integerTwo << " is " << ((integerOne / integerTwo) * 100) << "%.";

			}
			else if (selectionTwo == 7) {

				std::cout << "\n\n1. Cos"
						  << "\n2. Tan"
						  << "\n3. Logarithm"
						  << "\n4. Decadic Logarithm"
						  << "\n5. Asin"
						  << "\n6. Acos"
						  << "\n\n7. Page 4"
						  << "\n\nPlease select the number of your operation: ";
				std::cin >> selectionThree;

				if (selectionThree == 1) {

					std::cout << "\nEnter number: ";
					std::cin >> integerOne;
					std::cout << "\n" << integerOne << " = " << "cos(" << cos(integerOne) << ")";

				}
				else if (selectionThree == 2) {
				
					std::cout << "\nEnter number: ";
					std::cin >> integerOne;
					std::cout << "\n" << integerOne << " = " << "tan(" << tan(integerOne) << ")";
				
				}
				else if (selectionThree == 3) {
				
					std::cout << "\nEnter number: ";
					std::cin >> integerOne;
					std::cout << "\n" << integerOne << " = " << "log(" << log(integerOne) << ")";

				}
				else if (selectionThree == 4) {

					std::cout << "\nEnter number: ";
					std::cin >> integerOne;
					std::cout << "\n" << integerOne << " = " << "log10(" << log10(integerOne) << ")";
					
				}
				else if (selectionThree == 5) {

					std::cout << "\nEnter sin: ";
					std::cin >> integerOne;
					std::cout << "\n" << integerOne << " = " << "asin(" << asin(integerOne) << ")";

				}
				else if (selectionThree == 6) {

					std::cout << "\nEnter cos: ";
					std::cin >> integerOne;
					std::cout << "\n" << integerOne << " = " << "acos(" << acos(integerOne) << ")";

				}
				else if (selectionThree == 7) {

					std::cout << "\n\n1. Atan"
							  << "\n2.[Probably will not exist]"
							  << "\n3.[Probably will not exist]"
							  << "\n4.[Probably will not exist]"
							  << "\n5.[Probably will not exist]"
							  << "\n6.[Probably will not exist]"
							  << "\n\nSelect the number of your operation: ";
					std::cin >> selectionFour;

					if (selectionFour == 1) {

						std::cout << "\nEnter tan: ";
						std::cin >> integerOne;
						std::cout << "\n" << integerOne << " = " << "atan(" << atan(integerOne) << ")";

					}
					else if (selectionFour == 2) {


	
					}
					else if (selectionFour == 3) {



					}
					else if (selectionFour == 4) {


						
					}
					else if (selectionFour == 5) {


						
					}
					else if (selectionFour == 6) {



					}
				}
				else
				{

					std::cout << "\n\nYou have entered a character or characters that is/are not on the list.";

				}
			}
			else if (selectionTwo == 8) {

				

			}
			else {

				std::cout << "\n\nYou have entered a character or characters that is/are not on the list.";

			}

		}
		else if (selection == 8) {

			

		}
		else {

			std::cout << "You have entered a character or characters that is/are not on the list.";

		}


		std::cout << "\n\n-------------------------------------------------------------------------------------------------\n\n\n";

	}
}
